using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEditor.iOS.Xcode.Extensions;
using System.IO;
using System.Diagnostics;

public class FairBidPostBuild : MonoBehaviour
{
    [PostProcessBuild(101)]
    private static void OnPostProcessBuildPlayer(BuildTarget target, string pathToBuiltProject)
    {
        if (target != BuildTarget.iOS)
        {
            return;
        }

        UnityEngine.Debug.Log("FairBid: started post-build script");

        XcodeProject xcodeProject = new XcodeProject(pathToBuiltProject);
        FairBidPostBuild.UpdateProjectSettings(xcodeProject);
        FairBidPostBuild.EmbedFramework(xcodeProject);
        FairBidPostBuild.InsertBuildPhase(xcodeProject);
        xcodeProject.Save();

        UnityEngine.Debug.Log("FairBid: finished post-build script");
    }

    private static void UpdateProjectSettings(XcodeProject xcodeProject)
    {
        xcodeProject.AddBuildProperty("OTHER_LDFLAGS", "-ObjC");
        xcodeProject.SetBuildProperty("CLANG_ENABLE_MODULES", "YES");
        xcodeProject.SetBuildProperty("LD_RUNPATH_SEARCH_PATHS", "$(inherited) @executable_path/Frameworks");
    }

    private static void EmbedFramework(XcodeProject xcodeProject)
    {
        const string frameworkPath = "Frameworks/FairBid/Plugins/iOS";
        const string frameworkName = "FairBidSDK.framework";
        xcodeProject.EmbedFramework(frameworkName, frameworkPath);
    }

    private static void InsertBuildPhase(XcodeProject xcodeProject)
    {
        string name = "FairBidSDK - Strip Unused Architectures";
        string shellScript = "bash ${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}/FairBidSDK.framework/strip-frameworks.sh";
        xcodeProject.InsertShellScriptBuildPhase(name, shellScript);
    }
}

class XcodeProject
{
    private PBXProject pbxProject;
    private string xcodeProjectPath;
    private string targetGUID;

    public XcodeProject(string projectPath)
    {
        xcodeProjectPath = projectPath + "/Unity-iPhone.xcodeproj/project.pbxproj";
        Open();
    }

    private void Open()
    {
        pbxProject = new PBXProject();
        pbxProject.ReadFromFile(xcodeProjectPath);

    #if UNITY_2019_3_OR_NEWER
        targetGUID = pbxProject.GetUnityMainTargetGuid();
    #else
        targetGUID = pbxProject.TargetGuidByName("Unity-iPhone");
    #endif
    }

    public void Save()
    {
        pbxProject.WriteToFile(xcodeProjectPath);
    }

    public void AddBuildProperty(string name, string value)
    {
        pbxProject.AddBuildProperty(targetGUID, name, value);
    }

    public void SetBuildProperty(string name, string value)
    {
        pbxProject.AddBuildProperty(targetGUID, name, value);
    }

    public void EmbedFramework(string name, string path)
    {
        string framework = Path.Combine(path, name);
        string fileGuid = pbxProject.FindFileGuidByProjectPath(framework);
        pbxProject.AddFileToEmbedFrameworks(targetGUID, fileGuid);
    }

    public void InsertShellScriptBuildPhase(string name, string shellScript)
    {
    #if UNITY_2018_3_OR_NEWER
        string shellPath = "/bin/sh";
        int index = 1000;
        pbxProject.InsertShellScriptBuildPhase(index, targetGUID, name, shellPath, shellScript);
    #else
        //The project must be saved and opened again because the python script will modify it
        Save();
        InsertShellScriptBuildPhaseFallback(shellScript);
        Open();
    #endif
    }

    private void InsertShellScriptBuildPhaseFallback(string shellScript)
    {
        string pythonScript = "FairBid/Editor/FairBidPostprocessBuildPlayer.py";
        string scriptPath = Path.Combine(Application.dataPath, pythonScript);
        if (!File.Exists(scriptPath))
        {
            UnityEngine.Debug.LogError("FairBid: couldn't find script at path: " + scriptPath + ". Did you accidentally delete it?");
            return;
        }

        string args = string.Format("\"{0}\" \"{1}\" \"{2}\"", scriptPath, xcodeProjectPath, shellScript);
        UnityEngine.Debug.Log(string.Format("FairBid: starting FairBidPostprocessBuildPlayer with args: {0}", args));
        ExecutePythonScript(args);
    }

    private void ExecutePythonScript(string args)
    {
        ProcessStartInfo startInfo = new ProcessStartInfo
        {
            FileName = "python2.7",
            Arguments = args,
            UseShellExecute = false,
            RedirectStandardOutput = false,
            CreateNoWindow = true
        };

        Process proc = new Process
        {
            StartInfo = startInfo
        };
        proc.Start();
        proc.WaitForExit();
        if (proc.ExitCode > 0)
        {
            UnityEngine.Debug.LogError("FairBid: post-build script had an error(code=" + proc.ExitCode + ")");
        }
    }
}
