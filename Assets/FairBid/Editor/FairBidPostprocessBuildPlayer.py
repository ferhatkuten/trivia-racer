#!/usr/bin/python
import sys
from FYB_mod_pbxproj import XcodeProject

def edit_pbxproj_file():
    try:
        pbxproj = sys.argv[1]
        print "FairBidPostProcessBuildPlayer: Loading project at path: ", pbxproj
        project = XcodeProject.Load(pbxproj)

        script = sys.argv[2]
        print "FairBidPostProcessBuildPlayer: Adding run script: ", script
        project.add_run_script("Unity-iPhone", script, False)

        project.save()
        print "FairBidPostProcessBuildPlayer: Successfully modified project"
    except Exception as e:
      print "FairBidPostProcessBuildPlayer: Failed with error: ", e
      return 1

sys.exit(edit_pbxproj_file())
