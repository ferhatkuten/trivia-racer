//
// FairBid Unity SDK
//
// Copyright (c) 2019 Fyber. All rights reserved.
//
namespace Fyber
{
    /// <summary>
    /// Defines different genders an user can have.
    /// </summary>
    public enum Gender
    {
        /// <summary>
        /// Unknown.
        /// </summary>
        UNKNOWN = 0,

        /// <summary>
        /// Male.
        /// </summary>
        MALE,

        /// <summary>
        /// Female.
        /// </summary>
        FEMALE,

        /// <summary>
        /// Other.
        /// </summary>
        OTHER
    }

}
